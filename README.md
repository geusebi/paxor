# paxor

Simple toy program to split file into n parts and have a parity file.

Losing one file or the parity still gives you the chance to reconstruct
the original.

```
./autogen.sh
./configure
make
make run-test
```

