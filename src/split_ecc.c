#include <config.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>
#include <args.h>
#include <ecc.h>


const char *argp_program_version = "split_ecc " VERSION;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

char doc[] = 
    "split_ecc -- Split a file"
    "\v"
    "Create `n' files from one input and store the data "
    "interleaving the blocks.";


int
main(int argc, char *argv[])
{
    int i;
    FILE *input, **files;
    char *path, *path_fmt, *block;
    
    
    // Parse arguments
    struct split_args args;
    
    if (split_arg_parse(argc, argv, &args, doc) != 0) {
        return EXIT_FAILURE;
    }
    
    if (args.verbose) {
        fprintf(stderr, "Arguments\n");
        fprintf(stderr, "  nparts: %d\n", args.nparts);
        fprintf(stderr, "  force: %s\n", args.force ? "yes" : "no");
        fprintf(stderr, "  verbose: %s\n", args.verbose ? "yes" : "no");
        fprintf(stderr, "  input: %s\n", args.input ? args.input : "<stdin>");
        fprintf(stderr, "  output: %s\n", args.output);
    }
    
    
    // INPUT is either a valid filepath or NULL (stdin)
    if (args.input == NULL) {
        if (args.verbose) {
            fprintf(stderr, "Reading from standard input (stdin)\n");
        }
        input = stdin;
    } else {
        if (args.verbose) {
            fprintf(stderr, "Opening input file: %s\n", args.input);
        }
        input = fopen(args.input, "r");
    }
    
    if (input == NULL) {
        perror("Could not open input file");
        return EXIT_FAILURE;
    }
    
    
    // Open all output files
    
    // Output files will be in the form of filename.01, filename.02 ...
    // i.e. original filename - dot - zero padded part number
    //
    // `path` is reused for all files and needs to be long enough to 
    // contain the longest filename
    
    int part_digits = floor(log10(args.nparts)) + 1;
    path = malloc(
        strlen(args.output) + // base filename
        1 +                   // dot
        part_digits +         // number
        1                     // null byte
    );
    
    // Please ignore (I know, it was probably unnecessary)
    int path_fmt_digits = floor(log10(part_digits)) + 1;
    path_fmt = malloc(
        5 +                 // "%s.%0"
        path_fmt_digits +   // please ignore :-)
        1 +                 // "d"
        1                   // null byte
    );
    sprintf(path_fmt, "%%s.%%0%dd", part_digits);
    
    // Actual file opening
    files = malloc(sizeof(*files) * args.nparts);
    for (i = 0; i < args.nparts; i++) {
        sprintf(path, path_fmt, args.output, i);
        files[i] = fopen(path, "w");
        if (args.verbose) {
            fprintf(stderr, "part: %s\n", path);
        }
    }
    
    
    // Split input over outputs
    // data is interleaved in blocks of size BSIZE 
    block = calloc(BSIZE, sizeof(char));
    i = 0;
    
    while (!feof(input)) {
        for (i = 0; i < args.nparts; i++) {
            read_pad_block(input, block, BSIZE);
            fwrite(block, 1, BSIZE, files[i]);
        }
    }
    
    
    // Close all files
    fclose(input);
    for (i = 0; i < args.nparts; i++) {
        fclose(files[i]);
    }
    
    
    // Clean up
    free(args.input);
    free(args.output);
    free(path);
    free(path_fmt);
    free(files);
    free(block);
    
    return EXIT_SUCCESS;
}
