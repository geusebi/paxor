#include <config.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <args.h>
#include <ecc.h>


const char *argp_program_version = "join_ecc " VERSION;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

char doc[] = 
    "join_ecc -- Rebuild original file"
    "\v"
    "Rebuild the original file from all the parts.";


int
main(int argc, char *argv[])
{
    int nparts, i, len;
    FILE **files, *output;
    char *block, **cursor, last_char;
    
    
    // Parse arguments
    struct join_args args;
    
    if (join_arg_parse(argc, argv, &args, doc) != 0) {
        return EXIT_FAILURE;
    }
    
    if (args.verbose) {
        fprintf(stderr, "Arguments\n");
        fprintf(stderr, "  force: %s\n", args.force ? "yes" : "no");
        fprintf(stderr, "  verbose: %s\n", args.verbose ? "yes" : "no");
        fprintf(stderr, "  output: %s\n", args.output);
        
        fprintf(stderr, "  parts:");
        cursor = args.filenames;
        while (*cursor != NULL) {
            fprintf(stderr, " '%s'", *(cursor++));
        }
        fprintf(stderr, "\n");
    }
    
    // Open all part files and the output file
    
    // Files pool
    files = malloc(sizeof(*files) * args.nfiles);
    
    cursor = args.filenames;
    for (i = 0, nparts = 0; i < args.nfiles; i++, cursor++) {
        last_char = (*cursor)[strlen(*cursor) - 1];
        if (last_char < '0' || last_char > '9') {
            // By convention if the last character is not a digit
            // skip the file
            if (args.verbose) {
                fprintf(stderr, "skip: %s\n", *cursor);
            }
            continue;
        }
        
        files[nparts] = fopen(*cursor, "r");
        if (args.verbose) {
            fprintf(stderr, "part: %s\n", *cursor);
        }
        if (files[nparts] == NULL) {
            perror("Could not open one or more parts files");
            return EXIT_FAILURE;
        }
        ++nparts;
    }
    
    // Update nfiles in case there is some extraneous file e.g. ".p"
    files = realloc(files, sizeof(*files) * nparts);
    
    
    // Open output file
    if (args.verbose) {
        fprintf(stderr, "rebuilt: %s\n", argv[argc - 1]);
    }
    
    output = fopen(args.output, "w");
    
    if (output == NULL) {
        perror("Could not open output file");
        return EXIT_FAILURE;
    }
    
    
    // Rebuild original file
    block = calloc(BSIZE, sizeof(char));
    
    i = 0;
    while (!feof(files[0])) {
        len = read_strip_block(files[i], block, BSIZE);
        if (len > 0) { // Just to be safe
            fwrite(block, 1, len, output);
        }
        i = (i + 1) % nparts;
    }
    
    // Close all files and clean up
    
    fclose(output);
    free(args.output);
    
    for (i = 0; i < nparts; i++) {
        fclose(files[i]);
    }
    free(files);
    
    free(block);
    
    return EXIT_SUCCESS;
}

