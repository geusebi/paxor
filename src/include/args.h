#ifndef ECC_ARGS_H
#define ECC_ARGS_H

#include <argp.h>

struct split_args {
    int verbose;
    int force;
    int nparts;
    char *input;
    char *output;
};

struct parity_join_args {
    int verbose;
    int force;
    int nfiles;
    char **filenames;
    char *output;
};

#define parity_args parity_join_args
#define join_args parity_join_args


error_t
split_arg_parse(int argc, char *argv[], struct split_args *args, char doc[]);

error_t
split_parse_opt(int key, char *arg, struct argp_state *state);


error_t
parity_join_arg_parse(int argc, char *argv[], struct parity_args *args, char doc[]);

#define parity_arg_parse parity_join_arg_parse
#define join_arg_parse parity_join_arg_parse

error_t
parity_join_parse_opt(int key, char *arg, struct argp_state *state);

#define parity_parse_opt parity_join_parse_opt
#define join_parse_opt parity_join_parse_opt


int
safe_atoi(char *nptr);

#endif
