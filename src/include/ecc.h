#ifndef ECC_H
#define ECC_H

#include <stdio.h>

#define BSIZE 256


void
read_pad_block(FILE *fh, char *block, size_t nmemb);

int
read_strip_block(FILE *fh, char *block, size_t nmemb);

void
update_parity_block(char *block, char *parity, size_t nmemb);

#endif
