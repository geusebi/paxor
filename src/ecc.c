#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stdbool.h>
#include <errno.h>
#include <math.h>
#include "ecc.h"


void
update_parity_block(char *block, char *parity, size_t nmemb)
{
    for (size_t i = 0; i < nmemb; i++) {
        parity[i] ^= block[i];
    }
}


void
read_pad_block(FILE *fh, char *block, size_t nmemb)
{
    size_t read, npad;
    read = fread(block, 1, nmemb - 1, fh);
    
    for (npad = 0; read < nmemb; npad++, read++) {
        npad = (npad <= 127) ? npad : 127;
        block[read] = npad;
    }
}


int
read_strip_block(FILE *fh, char *block, size_t nmemb)
{
    size_t read;
    read = fread(block, 1, nmemb, fh);
    
    if (read < 1) { // Probably EOF
        return 0;
    }
    
    while (read > 0 && block[read - 1] != '\0') {
        read -= block[read - 1];
    }
    return read - 1;
}

