#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <argp.h>
#include <args.h>


char args_doc[] = "-o OUTPUT INPUTS...";


struct argp_option options[] = {
    {"output"    , 'o', "OUTPUT", OPTION_NO_USAGE, "Output file name"                                 , 0},
    {"force"     , 'f',        0,               0, "Overwrite output without asking (not implemented)", 2},
    {"verbose"   , 'v',        0,               0, "Produce verbose output"                           , 2},
    {0}
};


error_t
parity_join_arg_parse(
    int argc, char *argv[],
    struct parity_args *args, char doc[])
{
    // Defaults
    args->verbose   = 0;
    args->force     = 0;
    args->nfiles    = 0;
    args->filenames = NULL;
    args->output    = NULL;
    
    struct argp argp = {options, parity_parse_opt, args_doc, doc, NULL, NULL, NULL};
    
    return argp_parse(&argp, argc, argv, 0, 0, args);
}


error_t
parity_join_parse_opt(int key, char *arg, struct argp_state *state)
{
    struct parity_args *args = state->input;

    switch (key) {
        case 'o':
            args->output = strdup(arg);
            break;
        case 'f':
            args->force = 1;
            break;
        case 'v':
            args->verbose = 1;
            break;
        
        case ARGP_KEY_ARGS:
            args->filenames = state->argv + state->next;
            args->nfiles = state->argc - state->next;
            break;
        
        case ARGP_KEY_NO_ARGS:
            argp_error(state, "Too few arguments");
            break; // Unnecessary - compiler was complaining
        
        case ARGP_KEY_END:
            if (args->output == NULL) {
                argp_error(state, "Missing output name");
            }
            break;
        
        default:
            return ARGP_ERR_UNKNOWN;
    }

    return 0;
}
