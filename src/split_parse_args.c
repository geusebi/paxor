#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <argp.h>
#include <args.h>


char args_doc[] = "[INPUT]";


struct argp_option options[] = {
    {"num-parts" , 'n', "NPARTS", 0, "Number of parts to split into (default: 3)"       , 0},
    {"output"    , 'o', "OUTPUT", 0, "Name pattern for output files (default: INPUT)"   , 0},
    {"force"     , 'f',        0, 0, "Overwrite output without asking (not implemented)", 2},
    {"verbose"   , 'v',        0, 0, "Produce verbose output"                           , 2},
    {0}
};


error_t
split_arg_parse(int argc, char *argv[], struct split_args *args, char doc[])
{
    // Defaults
    args->verbose = 0;
    args->force   = 0;
    args->nparts  = 3;
    args->input   = NULL;
    args->output  = NULL;
    
    struct argp argp = {options, split_parse_opt, args_doc, doc, NULL, NULL, NULL};
    
    return argp_parse(&argp, argc, argv, 0, 0, args);
}


error_t
split_parse_opt(int key, char *arg, struct argp_state *state)
{
    struct split_args *args = state->input;

    switch (key) {
        case 'n':
            args->nparts = safe_atoi(arg);
            if (errno) {
                argp_failure(state, 1, 0,
                    "Parts must be an integer");
            }
            if (args->nparts < 1) {
                argp_failure(state, 1, 0,
                    "Parts number must be greater than zero");
            }
            break;
        case 'o':
            args->output = strdup(arg);
            break;
        case 'f':
            args->force = 1;
            break;
        case 'v':
            args->verbose = 1;
            break;
        
        case ARGP_KEY_NO_ARGS:
            args->input = NULL;
            break;
        
        case ARGP_KEY_ARG:
            if (state->arg_num == 0) {
                args->input = strdup(arg);
                break;
            }
            // Fallthrough
        
        default:
            return ARGP_ERR_UNKNOWN;
        
        case ARGP_KEY_END:
            if (args->output == NULL) {
                args->output = strdup(
                    args->input ? args->input : "stdin"
                );
            }
            break;
    }

    return 0;
}


int
safe_atoi(char *nptr)
{
    int n;
    char *endptr;
    n = strtol(nptr, &endptr, 10);
    
    if (*nptr != '\0' && *endptr == '\0') {
        return n;
    }
    
    errno = EINVAL;
    return INT_MIN; // Return something astonishingly wrong
}
