#include <config.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <args.h>
#include <ecc.h>


const char *argp_program_version = "parity_ecc" VERSION;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

char doc[] = 
    "parity_ecc -- Create a parity file"
    "\v"
    "Create a parity file from the input files.";


int
main(int argc, char *argv[])
{
    int i, len;
    FILE **files, *parity;
    char **cursor;
    char *block, *pblock;
    
    
    // Parse arguments
    struct parity_args args;
    
    if (parity_arg_parse(argc, argv, &args, doc) != 0) {
        return EXIT_FAILURE;
    }
    
    if (args.verbose) {
        fprintf(stderr, "Arguments\n");
        fprintf(stderr, "  force: %s\n", args.force ? "yes" : "no");
        fprintf(stderr, "  verbose: %s\n", args.verbose ? "yes" : "no");
        fprintf(stderr, "  output: %s\n", args.output);
        
        cursor = args.filenames;
        for (i = 0; i < args.nfiles; i++) {
            fprintf(stderr, "part: %s\n", *(cursor+i));
        }
    }
    
    // Open parity file and all the input files
    
    parity = fopen(args.output, "w");
    if (args.verbose) {
        fprintf(stderr, "output: %s\n", args.output);
    }
    
    if (parity == NULL) {
        perror("Could not open parity file");
        return EXIT_FAILURE;
    }
    
    // Files pool
    files = malloc(sizeof(*files) * args.nfiles);
    
    cursor = args.filenames;
    for (i = 0; i < args.nfiles; i++) {
        files[i] = fopen(*cursor++, "r");
        if (files[i] == NULL) {
            perror("Could not open one or more parts files");
            return EXIT_FAILURE;
        }
    }
    
    
    // Create the parity file
    block = malloc(sizeof(char) * BSIZE);
    pblock = malloc(sizeof(char) * BSIZE);
    len = 0;
    
    while (!feof(files[0])) {
        memset(pblock, 0, BSIZE);
        memset(block, 0, BSIZE);
        for (i = 0; i < args.nfiles; i++) {
            len = fread(block, 1, BSIZE, files[i]);
            // Reached eof (probably the first file)
            if (len == 0) { 
                break;
            }
            update_parity_block(block, pblock, BSIZE);
        }
        // If one file is at the end it's either successfully created or
        // either a part file is corrupted
        if (len != 0) {
            fwrite(pblock, 1, BSIZE, parity);
        }
    }
    
    // Todo: handle end of files to check if the file has been created
    //       correctly 
    
    // Close all files and clean up
    
    fclose(parity);
    free(args.output);
    
    for (i = 0; i < args.nfiles; i++) {
        fclose(files[i]);
    }
    free(files);
    
    free(block);
    free(pblock);
    
    return EXIT_SUCCESS;
}
