#!/usr/bin/env bash

echo "# Split the file 'original' in 8 parts"
echo "$ split_ecc -n 8 original"
split_ecc -n 8 original
echo


echo "# Create the parity part file"
echo "$ parity_ecc -o original.p original.*"
parity_ecc -o original.p original.*
echo


echo "# Delete a part file (no. 3)"
echo "$ rm original.3"
rm original.3
echo


echo "# Rebuild the missing part"
echo "$ parity_ecc -o original.3 original.*"
parity_ecc -o original.3 original.*
echo


echo "# Rebuild the original file"
echo "$ join_ecc -o rebuilt original.*"
join_ecc -o rebuilt original.*
echo


echo "# Check if 'original' and 'rebuilt' files are identical"
echo "$ md5sum original rebuilt"
md5sum original rebuilt
echo

echo "# :-)"
